
//for
const nfinal = 12
for (var index = 0; index < nfinal; index++) {
    console.log(index);
}
console.log("imprimiendo array");
var s = [1,"hoa",122,2.33,{nombre:"Francisco"}]
for (let index = 0; index < s.length; index++) {
    console.log(s[index]);
}

//while
var x = 50
console.log("while");
while (x < 20) {
    console.log(x);
    x++
}

var y = 50
console.log("dowhile");
do {
    console.log(y);
    y++
} while (y < 20);


//ciclos de arrays
//https://www.w3schools.com/jsref/jsref_obj_array.asp
const h = [1,2,3,"ssss",{s:1},[1,2,3]]

h.forEach((element,index)=>{
    console.log("index",index,"element",element);
})


