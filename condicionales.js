//condicionales
//https://www.w3schools.com/js/js_comparisons.asp
const x = 2
//if
if(x == 1){
    console.log("codigo que se ejecuta");
}else{
    console.log("no se cumplio la condicion");
}
//if else
if(x == 1){

}else if(x == 2){
    
}else if (x == 3){

}else{

}

const y = 22
//swith
switch (y) {
    case 1:
        console.log("y1");
        break;
    case 2:
        console.log("y2");
        break;
    case 3:
        console.log("y3");
        break;
    default:
        console.log("default");
        break;
}




//swith con object

const validador = {
    "1":()=>console.log(1),
    "2":()=>console.log(2),
    "3":()=>console.log(3),
    "4":()=>console.log(4),
}
const u = "22was"
const r = validador[u] || (() => console.log("default"))
r()



const t = 1
if(t == 1){
    console.log("t1");
}
if(t => 1){
    console.log("t1=>");
}
