var x = 5

//operaciones matematicas
//https://www.w3schools.com/js/js_arithmetic.asp
console.log(x * 2);
console.log(x / 2);
console.log(x + 2);
console.log(x - 2);
console.log(x % 2);
console.log(x ** 2);//5 elevado 2

//contadores
console.log("contadores");
x = x + 1
console.log(x);
x = x - 2
console.log(x);
x = x / 2
console.log(x);
x = x * 5
console.log(x);

x = 5
x += 1
console.log(x);
x -= 2
console.log(x);
x /= 2
console.log(x);
x *= 5
console.log(x);

var y = 1

y++
console.log(y);
y--
console.log(y);


//string
//https://www.w3schools.com/jsref/jsref_charat.asp
var s = "hola"
var d = "todos"
//concatenar string
var f = s+" "+d
console.log(f);
s = s + " asss "
s += " adad "
console.log(s+"hsss");

console.log(f.charAt(1));


