
//declarar variables
var nombreVaiable = 1

//declarar constatntes
const nombreConstante = 1

//esto si se puede hacer
nombreVaiable = 3

//esto no se puede hacer
//nombreConstante = 3

//tipos de varibles

//number
var x = 1
var y = 1.2
//string
var s = 'hola'
var d = "hola2"
var f = `hola3 ${s}`
//boolean
var q = true
var w = false

//sobreescribir variables
q = "hola"
//console.log(q);

//Array
//https://www.w3schools.com/js/js_array_methods.asp
var a = ["hoa",1,2,3,true,1.2]
console.log(a[0]);
console.log("numero de elemetos de array",a.length);
a[0] = "Hola a todos"
console.log(a[0]);
console.log("numero de elemetos de array",a.length);
a = ["ssss"]
console.log(a[0]);
console.log("numero de elemetos de array",a.length);

//object
var j = {
    nombre : "francisco",
    edad : 24,
    dni : 25982283
}
console.log(j);
j.nombre = "ssss"
console.log(j);
j = {
    edad : 24
}
console.log(j);
console.log(j.nombre);